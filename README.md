# librarian

_A library is a curated collection of sources of information and similar resources, selected by experts and made accessible to a defined community for reference or borrowing, ..._ [source](https://en.wikipedia.org/wiki/Library)

